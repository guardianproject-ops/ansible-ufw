# ufw

Installs and configures the ufw firewall tool on debian based systems.

OpenSSH/port 22 is *always allowed* to prevent lockouts.

**WARNING**: ufw does not play well with Docker. Do not use this role when
docker is installed on the same host. Refer to
[guardianproject-ops/ansible-firewall-docker](https://gitlab.com/guardianproject-ops/ansible-firewall-docker)
instead.

## Requirements

* Debian

## Role Variables

```
ufw_open_ports_tcp: []

# reset/clear all firewall rules (breaks idempotency)
ufw_reset: False

ufw_enable_ipv6: False
```


## Example Playbook

```yaml
- hosts: servers
  vars:
    ufw_open_ports_tcp:
      - 80
    ufw_enable_ipv6: True
  roles:
    - ansible-ufw
```

License
-------

[GNU Affero General Public License (AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html)

Author Information
------------------

* Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)
